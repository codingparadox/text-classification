# text-classification

A naive text classification module.

## Setup
`python` version 3+ is required. The basic requirements can be installed using the requirements file as:  
```bash
pip install -r requirements.txt
```

## Data
The data is in the `data/` directory.

## Train
Run `trainer.py`.  
```bash
python trainer.py
```

Following steps are done:  
- load data
- split into training and validation set
- create feature extractor
- save feature extractor
- create model
- save model
- evaluate with validation set

## Train
Run `test.py`.  
```bash
python test.py
```

Following steps are done:  
- load test data
- load feature extractor
- load model
- predict
