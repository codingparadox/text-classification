#!/usr/bin/env python3

import numpy as np

from sklearn.metrics import (
    confusion_matrix, precision_score, classification_report
)

def get_confusion_matrix(target, predictions):
    return confusion_matrix(target, predictions)

def calculate_accuracy(target, predictions):
    return np.sum((target==predictions))/len(target)

def get_classification_report(target, predictions):
    return classification_report(target, predictions)


def main():
    pass

if __name__ == "__main__":
    main()

