#!/usr/bin/env python3

import pickle
from sklearn.feature_extraction.text import TfidfVectorizer

class TfIdfFeatureExtractor:
    def __init__(self):
        self.vectorizer = None

    def fit(self, texts):
        self.vectorizer = TfidfVectorizer(ngram_range=(1, 2)).fit(texts)
        return self.vectorizer

    def extract_features(self, texts):
        return self.vectorizer.transform(texts).toarray()

    def save(self, filename):
        print("Saving Feature Extractor to :: {}".format(filename))
        if not self.vectorizer:
            print("No Vectorizer Model Yet. Aborting...")
        with open(filename, 'wb') as f:
            pickle.dump(self, f, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def load(self, filename):
        print("Loading Feature Extractor from :: {}".format(filename))
        with open(filename, 'rb') as f:
            return pickle.load(f)


def main():
    texts = [
        "i am nish",
        "i am human"
    ]

    fe = TfIdfFeatureExtractor()
    fe.fit(texts)
    features = fe.extract_features(texts)
    print(features)

if __name__ == "__main__":
    main()

