#!/usr/bin/env python3

from sklearn.naive_bayes import MultinomialNB
import pickle

class MLModel:
    def __init__(self):
        self.model = MultinomialNB()

    def train(self, X, Y):
        self.model.fit(X, Y)
        return self.model.predict(X)

    def predict(self, X):
        return self.model.predict(X)

    def save(self, filename):
        print("Saving model to :: {}".format(filename))
        with open(filename, 'wb') as f:
            pickle.dump(self, f, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def load(cls, filename):
        print("Loading model from:: {}".format(filename))
        with open(filename, 'rb') as f:
            return pickle.load(f)

    def __str__(self):
        return str(self.model.__class__)


def main():
    pass

if __name__ == "__main__":
    main()

