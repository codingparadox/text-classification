#!/usr/bin/env python3

import pandas as pd
from model import MLModel
from feature_extractor import TfIdfFeatureExtractor


def main():
    data = pd.read_csv('data/train_set.csv', encoding='ISO-8859-1')

    fe = TfIdfFeatureExtractor.load('fe.pkl')
    X = fe.extract_features(data['text'])

    model = MLModel.load('model.pkl')

    predictions = model.predict(X)
    print(predictions)

if __name__ == "__main__":
    main()

