#!/usr/bin/env python3
"""
    The main module to train everything.
    For now, no preprocessing done on the text.
"""

import pandas as pd
from sklearn.model_selection import train_test_split

from feature_extractor import TfIdfFeatureExtractor
from evaluator import (
    calculate_accuracy,
    get_confusion_matrix,
    get_classification_report
)
from model import MLModel


def main():
    print("Loading train data...")
    data = pd.read_csv('data/train_set.csv', encoding='ISO-8859-1')
    texts_train, texts_val, Y_train, Y_val = train_test_split(data['text'],
                                                        data['label'],
                                                        train_size=0.8
                                                    )

    print("Extracting features...")
    fe = TfIdfFeatureExtractor()
    fe.fit(texts_train)
    X_train = fe.extract_features(texts_train)
    X_val = fe.extract_features(texts_val)
    fe.save("fe.pkl")

    # train
    model= MLModel()
    print("Training using '{}'".format(model))
    model.train(X_train, Y_train)
    model.save('model.pkl')

    # predict
    model = MLModel()
    target = Y_val
    predictions = model.predict(X_val)

    # eval
    print("Evaluating...")
    print("Overall Accuracy :: {}".format(calculate_accuracy(target, predictions)))
    print("Confusion Matrix\n", get_confusion_matrix(target, predictions))
    print("Classification Report\n", get_classification_report(target, predictions))

    model.save('model.pkl')



if __name__ == "__main__":
    main()

